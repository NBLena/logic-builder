extends Polygon2D

export var on = false
export var disabled = false
onready var lever = get_node("Lever")
onready var offHighlight = get_node("OffHighlight")
onready var onHighlight = get_node("OnHighlight")
signal flipped(on)

func _ready():
	if not disabled:
		updateOutput()
	else:
		$Output.disabled = true
		$DragableArea.disabled = true

func _on_Area2D_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("ui_accept") and not disabled:
		on = not on
		updateOutput()

func updateOutput():
	emit_signal("flipped", on)
	onHighlight.visible = on
	offHighlight.visible = not on
	if on:
		lever.rotation_degrees = 45
		return
	lever.rotation_degrees = -45
	
func save():
	return {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"on": on,
		"connectedObjects": $Output.gatherIds()
	}
