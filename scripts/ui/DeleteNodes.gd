extends Polygon2D

func _on_Area2D_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("ui_accept"):
		self.color = Color(0.152941, 0.509804, 0.615686)
		Global.deleteAllElements()
	else:
		self.color = Color(0.184314, 0.035294, 0.105882)
