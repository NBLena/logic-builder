extends Node2D

var id = 0
var connectedOutput:Node2D = null
var time := 2.0
export var disabled := false
signal recieved(input)
signal passthrough(input)
signal respond(target, data)

func _ready()->void:
	add_to_group("connection_nodes")
	id = Global.getNextId()

func _process(delta):
	if(time/2 < 1):
		time += delta
		self.modulate = lerp(Color(1, 0.003922, 0.003922), Color(1,1,1), time / 2)

func _on_recieve(input)->void:
	if input is bool: # Binary value given by the last element
		emit_signal("recieved", input)
	elif input is Array: # Array with edges in graph, to test for cycles
		var lastConnection := [connectedOutput, self]
		if input.has(lastConnection):
			# Cycle detected! Abort ship!
			# Assumption: cycles are caught in the input node just after the offending connection
			connectedOutput.call("alertFlash")
			alertFlash()
			cancelOutput()
			return
		input.append(lastConnection)
		emit_signal("passthrough", input)
	elif input == "disconnect":
		disconnectOutput()
	elif input == "position":
		respondWithPosition()
	else:
		print("ERROR: Input recieved illegal data via on_recieve()")
	
func _on_Area2D_input_event(_viewport, event, _shape_idx)->void:
	if disabled:
		return
	# Finish connecting line
	if (event.is_action_released("ui_accept") or event.is_action_pressed("ui_accept")) and Global.selectedOutput != null and connectedOutput == null:
		connectOutput()
	# Delete line to this node
	elif event.is_action_released("ui_delete") and connectedOutput != null:
		cancelOutput()

func connectOutput(source=null)->void:
	if source == null:
		if Global.selectedOutput.name != 'Output':
			return
		connectedOutput = Global.selectedOutput
	else:
		connectedOutput = source
	if connect("respond", connectedOutput, "_on_response") != OK:
		print("ERROR: Input could not connect Respond signal to Output, ", self.name, " to ", connectedOutput)
	connectedOutput.finishConnection(self)

func disconnectOutput()->void:
	disconnect("respond", connectedOutput, "_on_response")
	connectedOutput = null
	emit_signal("recieved", false)

func cancelOutput()->void:
	if connectedOutput == null:
		return
	connectedOutput.disconnectOne(self)
	disconnectOutput()

func respondWithPosition()->void:
	emit_signal("respond", self, global_position)

func alertFlash()->void:
	self.modulate = Color(1, 0.003922, 0.003922)
	time = 0
	
# Prepare for deletion
func selfDestruct()->void:
	self.id = null
