extends Camera2D
var dragPosition:Vector2 = Vector2.INF	# Infinite stands in for null in this case
var startDragPosition:Vector2

func _input(event)->void:
	# Is Menu open? If yes, ignore all input
	if get_viewport().get_child(2).get_node("Camera2D").get_node("UI").has_node("SaveLoadMenu"):
		return
	# Zoom Camera
	if event.is_action_pressed("camera_zoom_in"):
		zoom -= Vector2(0.1,0.1)
	if event.is_action_pressed("camera_zoom_out"):
		zoom += Vector2(0.1,0.1)
	zoom.x = clamp(zoom.x, 0.2, 100)
	zoom.y = clamp(zoom.y, 0.2, 100)
	$UI.rect_scale = zoom
	# Drag Camera
	if event.is_action_pressed("camera_drag"):
		dragPosition = event.global_position
		startDragPosition = global_position
	if event.is_action_released("camera_drag"):
		dragPosition = Vector2.INF
	if event is InputEventMouseMotion and dragPosition != Vector2.INF:
		global_position = startDragPosition + (dragPosition - event.global_position) * zoom
