extends Node

onready var camera = get_viewport().get_child(2).get_node("Camera2D")

func _input(event):
	if event.is_action_pressed("quick_save"):
		if Global.savefileName != null:
			save_game(Global.savefileName)
	if event.is_action_pressed("quick_load"):
		if Global.savefileName != null:
			load_game(Global.savefileName)

func save_game(saveFilename):
	Global.savefileName = saveFilename
	var save_game = File.new()
	save_game.open(saveFilename, File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("elements")
	
	save_game.store_line(to_json({"nextId": Global.nextInputId}))
	save_game.store_line(to_json({"cam_x": camera.global_position.x, "cam_y": camera.global_position.y, "zoom": camera.zoom.x}))
	
	for i in save_nodes:
		if i.disabled:
			continue
		if not i.has_method("save"):
			print("ERROR: Could not save ", i.name, ", it has no save method!")
			continue
		var node_data = i.call("save")
		save_game.store_line(to_json(node_data))
	save_game.close()

func load_game(saveFilename):
	Global.savefileName = saveFilename
	var save_game = File.new()
	var ObjectsToConnect = [] #will be needed later
	var SumsToConnect = []
	if not save_game.file_exists(saveFilename):
		print("ERROR: No save file found!")
		return # Error! We don't have a save to load.

	# We need to reset the game state 
	Global.deleteAllElements()

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open(saveFilename, File.READ)
	print("Loading: ", saveFilename)
	while not save_game.eof_reached():
		var current_line = save_game.get_line()
		
		if current_line == null or current_line == '':
			continue
		
		# Parse the jason in the line to make it usable
		current_line = parse_json(current_line)
		
		
		# Restore global state
		if current_line.has("nextId"):
			Global.nextInputId = current_line["nextId"]
			continue
		if current_line.has("cam_x"):
			camera.global_position.x = current_line["cam_x"]
			camera.global_position.y = current_line["cam_y"]
			camera.zoom.x = current_line["zoom"]
			camera.zoom.y = current_line["zoom"]
			continue
		
		# Now, to the elements
		# Firstly, we need to create the object and add it to the tree and set its position.
		var new_object = load(current_line["filename"]).instance()
		get_node(current_line["parent"]).add_child(new_object)
		var newObjectPosition = Vector2(current_line["pos_x"], current_line["pos_y"])
		if new_object is Node2D:
			new_object.position = newObjectPosition
		elif new_object is Control:
			new_object.rect_position = newObjectPosition
		else:
			print("ERROR when loading object ", new_object.name ,"- its type was neither Node2D nor Control!")
			continue
		
		# Next we set the remaining variables.
		for i in current_line.keys():
			if i == "filename" or i == "parent" or i == "pos_x" or i == "pos_y":
				continue
			elif i == "connectedObjects":
				ObjectsToConnect.append(
					{	"outputNode": new_object.get_node("Output"),
						"inputIds": current_line["connectedObjects"]
					}
				)
			elif i == "inputObjectIds":
				# TODO is this necessary?
				#if not current_line["inputObjectIds"] is Array:
					#new_object.get_node("Input").set("id", current_line["inputObjectIds"])
					#continue
				new_object.get_node("Input").set("id", current_line["inputObjectIds"][0])
				for j in range(1,current_line["inputObjectIds"].size()):
					new_object.get_node(str("Input",j+1)).set("id", current_line["inputObjectIds"][j])
			elif i == "chainlinkIds": # Is a Sum Object
				SumsToConnect.append(
					{	"sumObject": new_object,
						"chainlinkIds": current_line["chainlinkIds"]
					}
				)
			elif i == "chainlinkId":
				new_object.get_node("Chainlink").set("id", current_line["chainlinkId"])
			else:
				new_object.set(i, current_line[i])

		if new_object.has_method("updateOutput"):
			new_object.call("updateOutput")

	# Finally iterate over output objects that were connected
	for object in ObjectsToConnect:
		if len(object["inputIds"]) > 0:
			object["outputNode"].call("reconnectAll", object["inputIds"])
	
	for sum in SumsToConnect:
		if len(sum["chainlinkIds"]) > 0:
			sum["sumObject"].call("reconnectAll", sum["chainlinkIds"])

	save_game.close()
