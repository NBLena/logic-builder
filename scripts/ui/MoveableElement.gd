extends Area2D

onready var parent = $".."
var dragging = false
export var disabled = false

func _on_DragableArea_input_event(_viewport, event, _shape_idx):
	if disabled:
		return
		
	if event.is_action_pressed("ui_accept") and Global.currentlyDraggedObject == null:
		Global.currentlyDraggedObject = self
		dragging = true
	if event.is_action_released("ui_delete"):
		if parent is Label and parent.name == "Sum":
			parent.unregisterAll()
		for child in parent.get_children():
			if child is Node2D:
				if "Output" in child.name:
					child.disconnectAll()
				# Do I have to disconnect the inputs? Since they are being deleted?
				if "Input" in child.name:
					child.cancelOutput()
				if "Chainlink" in child.name:
					child.unregister()
		parent.queue_free()

func _input(event):
	if disabled:
		return
		
	if event.is_action_released("ui_accept"):
		dragging = false
		Global.currentlyDraggedObject = null
	
	if event is InputEventMouseMotion and dragging:
		# Dragging it (along a grid):
		var newPosition =	Vector2(floor(get_global_mouse_position().x/Global.gridSize)*Global.gridSize,
									floor(get_global_mouse_position().y/Global.gridSize)*Global.gridSize)
		if parent is Node2D:
			parent.position = newPosition
		elif parent is Control:
			parent.rect_position = newPosition
		else:
			print("ERROR: Trying to move an object but can't detect it's type!")
		
		if parent is Label and parent.name == "Sum":
			parent.updateLines()
			return
		
		# Update all Inputs and Outputs for redrawing the lines
		for child in parent.get_children():
			if child is Node2D:
				if "Output" in child.name:
					child.emit_signal("transmit", "position")
					continue
				if "Input" in child.name:
					child.respondWithPosition()
					continue
				if "Chainlink" in child.name:
					child.updateLine()
					continue
