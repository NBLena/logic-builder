extends Area2D

export var focusElement:String = ""
var inFocus := false
var hovering := false

func _ready()->void:
	if focusElement == "":
		print("ERROR: LoseFocus script was set but not a focusElement! Parent: ", get_parent())

func _input(event)->void:
	if event is InputEventMouseButton and event.pressed and inFocus and not hovering:
		get_parent().get_node(focusElement).release_focus()


func _on_FocusArea_mouse_entered():
	hovering = true


func _on_FocusArea_mouse_exited():
	hovering = false

func _on_FocusElement_focus_entered():
	inFocus = true

func _on_FocusElement_focus_exited():
	inFocus = false
