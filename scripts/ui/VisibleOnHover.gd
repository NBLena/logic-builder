extends Area2D

# Show and hide elements that belong to the parent when hovering the mouse of the Area2D

export var disabled := false
var elementsToShow:Array = []

func _on_HoverArea_mouse_entered(_unused=null)->void:
	if disabled:
		return
	for element in elementsToShow:
		if get_parent().disabled == true:
			continue
		get_parent().get_node(element).visible = true


func _on_HoverArea_mouse_exited()->void:
	if disabled:
		return
	for element in elementsToShow:
		if get_parent().disabled == true:
			continue
		get_parent().get_node(element).visible = false
