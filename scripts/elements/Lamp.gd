extends Polygon2D

export var on := false
export var disabled := false
signal passthrough(on)
onready var valueLabel := get_node("ValueLabel")
var lastValue := false

func _ready()->void:
	$HoverArea.elementsToShow.append("Chainlink")
	if not disabled:
		updateOutput()
	else:
		$Input.disabled = true
		$Output.disabled = true
		$DragableArea.disabled = true
		lastValue = on

func updateOutput():
	if on:
		self.color = Color(0.780392, 0, 0.223529)
		valueLabel.text = "1"
		return
	self.color = Color(1, 0.741176, 0.411765)
	valueLabel.text = "0"

func _on_Input_recieved(input_on):
	on = input_on
	updateOutput()
	passToChainlink()
	emit_signal("passthrough", on)
	lastValue = on

func passToChainlink()->void:
	if on != lastValue:
		if $Chainlink.SumObject != null:
			$Chainlink.SumObject.call("updateSum", on, self)

func save():
	return {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"connectedObjects": $Output.gatherIds(),
		"inputObjectIds": [$Input.id],
		"chainlinkId": $Chainlink.id
	}
