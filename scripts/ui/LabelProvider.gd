extends Node2D

var cameraWasDragged := false
var element := load("res://scenes/elements/Label.tscn")

func _input(event)->void:
	if event.is_action_pressed("camera_drag"):
		cameraWasDragged = false
	if event.is_action_released("camera_drag"):
		if not cameraWasDragged:
			var newNode:Control = element.instance()
			get_tree().root.get_node("Global").add_child(newNode)
			newNode.rect_position = Vector2(floor(get_global_mouse_position().x/Global.gridSize)*Global.gridSize,
											floor(get_global_mouse_position().y/Global.gridSize)*Global.gridSize)
	if event is InputEventMouseMotion and not cameraWasDragged:
		cameraWasDragged = true
