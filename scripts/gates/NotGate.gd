extends GateBase

func _ready()->void:
	if not disabled:
		inputs.resize(1)
		inputs[0] = false
		updateOutput()

func _on_Input_A_recieved(input_on:bool)->void:
	inputs[0] = input_on
	updateOutput()

func transform()->bool:
	return not inputs[0]
