extends Label

var value = 0
var lamps := []
var lines := []
export var disabled = false

func register(lamp)->void:
	lamps.append(lamp)
	var maxValue = pow(2, lamps.size()-1)
	lamp.get_node("SumValueLabel").text = String(maxValue)
	lamp.get_node("SumValueLabel").visible = true
	# Add value if lamp is on
	if lamp.on:
		updateSum(lamp.on, lamp)
	# Create a line to the lamp
	var line = Line2D.new()
	line.scale = Vector2(0.25,0.25)
	line.add_point(Vector2(0,0))
	line.add_point((lamp.get_global_position() - rect_global_position)*2)
	line.default_color = Color(0.5,0.5,0.5)
	lines.append(line)
	add_child(line)
	# Add it to HoverAreas list
	$HoverArea.elementsToShow.append(line.name)
	# And finally let HoverArea handle the visiblity
	line.visible = false

func unregister(lamp)->void:
	# remove lamp reference
	var index = lamps.find(lamp)
	lamp.get_node("Chainlink").SumObject = null
	lamp.get_node("SumValueLabel").visible = false
	lamps.remove(index)
	# remove line to lamp
	$HoverArea.elementsToShow.remove($HoverArea.elementsToShow.find(lines[index].name))
	remove_child(lines[index])
	lines.remove(index)
	# If no lamps are registered, delete self
	if lamps.size() == 0:
		queue_free()
		return
	# Reorder Lamps
	var x = 0
	for lamp in lamps:
		lamp.get_node("SumValueLabel").text = String(pow(2, x))
		x = x+1

func unregisterAll()->void:
	for x in lamps.size():
		unregister(lamps[lamps.size()-1])

func reconnectAll(ids:Array)->void:
	for id in ids:
		for chainlink in get_tree().get_nodes_in_group("chainlinks"):
			if is_instance_valid(chainlink):
				if id == chainlink.id:
					chainlink.SumObject = self
					register(chainlink.get_parent())
					continue

func updateSum(on, lamp)->void:
	if on:
		value += pow(2, lamps.find(lamp))
	else:
		value -= pow(2, lamps.find(lamp))
	text = String(value)

func updateLines(_lamp=null)->void:
	if _lamp != null:
		lines[lamps.find(_lamp)].set_point_position(1, (_lamp.get_global_position() - rect_global_position)*2)
	for line in lines:
		line.set_point_position(1, (lamps[lines.find(line)].get_global_position() - rect_global_position)*2)

func gatherChainlinkIds()->Array:
	var ids := []
	for lamp in lamps:
		ids.append(lamp.get_node("Chainlink").id)
	return ids

func save():
	return {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : rect_position.x, # Vector2 is not supported by JSON
		"pos_y" : rect_position.y,
		"chainlinkIds" : gatherChainlinkIds()
	}
