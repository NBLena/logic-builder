extends Node2D

export var gridSize = 20
var nextInputId = 0
var selectedOutput = null
var savefileName = null
var currentlyDraggedObject = null

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		if not get_viewport().get_child(2).get_node("Camera2D").get_node("UI").has_node("SaveLoadMenu"):
			var saveloadMenu = load("res://scenes/ui/saveloadMenu.tscn").instance()
			saveloadMenu.show_on_top = true
			get_viewport().get_child(2).get_node("Camera2D").get_node("UI").add_child(saveloadMenu)

func deleteAllElements():
	for input in get_tree().get_nodes_in_group("inputs"):
		input.selfDestruct()
	
	for chainlink in get_tree().get_nodes_in_group("chainlinks"):
		chainlink.selfDestruct()
	
	for sum in get_tree().get_nodes_in_group("sums"):
		sum.unregisterAll()
	
	for node in get_tree().get_nodes_in_group("elements"):
		if node.disabled:
			continue
		node.disabled = true
		node.queue_free()

# Link to objects - output to input
func linkElements(source:Polygon2D, target:Polygon2D, port=1)->void:
	var output = source.get_node("Output")
	var targetInputName = "Input"
	if port > 1:
		targetInputName = str(targetInputName, port)
	var input = target.get_node(targetInputName)
	linkConnectionElements(output, input)
	
func linkConnectionElements(source:Node2D, target:Node2D)->void:
	source.call("enterEditMode")
	target.call("connectOutput", source)
	#source.call("finishConnection", target)

func getNextId()->int:
	nextInputId += 1
	return nextInputId
