extends Button

onready var fileListBox = $"..".get_node("FileListBox")

func _on_LoadButton_pressed():
	var selectedFileName = fileListBox.get_line(fileListBox.cursor_get_line())
	SaveManager.load_game("user://" + selectedFileName)
	get_parent().queue_free()
