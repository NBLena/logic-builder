extends Polygon2D

export var on = false
export var disabled = false
var resetIsOn = false
signal changed(on)
onready var valueLabel = get_node("ValueLabel")

func _ready():
	if not disabled:
		updateOutput()
	else:
		$Input.disabled = true
		$Input2.disabled = true
		$Output.disabled = true
		$DragableArea.disabled = true

func updateOutput():
	emit_signal("changed", on)
	if on:
		self.color = Color(0.780392, 0, 0.223529)
		valueLabel.text = "1"
		return
	self.color = Color(0.917647, 0.603922, 0.588235)
	valueLabel.text = "0"

# Set
func _on_Input_recieved(input_on):
	if input_on and not resetIsOn:
		on = true
		updateOutput()
		emit_signal("changed", on)

# Reset
func _on_Input2_recieved(input_on):
	resetIsOn = input_on
	if input_on:
		on = false
		updateOutput()
		emit_signal("changed", on)
		return

func save():
	return {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"on": on,
		"connectedObjects": $Output.gatherIds(),
		"inputObjectIds": [$Input.id, $Input2.id]
	}
