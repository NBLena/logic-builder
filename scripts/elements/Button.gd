extends Polygon2D

export var on := false
export var disabled := false
onready var pressyPart := get_node("PressyPart")
signal pressed(on)

func _ready()->void:
	if not disabled:
		updateOutput()
	else:
		$Output.disabled = true
		$DragableArea.disabled = true

func _on_Area2D_input_event(_viewport, event, _shape_idx)->void:
	if disabled:
		return
	if event.is_action_pressed("ui_accept"):
		on = true
		updateOutput()
	elif event.is_action_released("ui_accept"):
		on = false
		updateOutput()

func updateOutput()->void:
	emit_signal("pressed", on)
	if on:
		pressyPart.color = Color(0.917647, 0.603922, 0.588235)
		return
	pressyPart.color = Color(1, 0.741176, 0.411765)

func save()->Dictionary:
	return {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"connectedObjects": $Output.gatherIds()
	}


func _on_DragableArea_mouse_exited()->void:
	on = false
	updateOutput()
