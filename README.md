## Logic Builder (Project Title)

Build logic circuits with switches, logic gates and flip-flops!

Available for Windows, Mac and Linux, made with Godot 3.2

![Pre-Alpha 0.2](https://gitlab.com/NBLena/logic-builder/raw/master/screenshots/Release_Candidate.png)
