extends Panel

func _ready():
	readFileList()

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		queue_free()

func readFileList()->void:
	var fileList = null
	var dir = Directory.new()
	if dir.open("user://") != OK:
		print("ERROR: Could not open user:// directory!")
		
	dir.list_dir_begin(true, true)
	var file_name = dir.get_next()
	while file_name != "":
		if not dir.current_is_dir():
			if fileList == null:
				fileList = file_name
			else:
				fileList += "\n" + file_name
		file_name = dir.get_next()
	dir.list_dir_end()
	
	if fileList == null:
		$FileListBox.text = ""
		$LoadButton.disabled = true
		$DeleteButton.disabled = true
		return
	
	$FileListBox.text = fileList
