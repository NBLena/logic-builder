extends Area2D

export var elementPath: String = ""
var element = null
var dragging = false
var newNode = null

func _ready():
	if elementPath == "":
		print("ERROR: No element set!")
		return
	element = load(elementPath)
	if element == null:
		print ("ERROR: Path to element is incorrect!")
		return
	get_child(0).set("disabled", true)

func _on_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("ui_accept"):
		dragging = true

func _input(event):
	if event.is_action_released("ui_accept"):
		dragging = false
		if newNode != null:
			newNode.z_index = 0
		newNode = null
	if event is InputEventMouseMotion:
		if dragging:
			if newNode == null:	# Not yet created? Now's the time!
				newNode = element.instance()
				newNode.z_index = 4096
				get_tree().root.get_node("Global").add_child(newNode)
			# Dragging it (along a grid):
			newNode.position = Vector2(	floor(get_global_mouse_position().x/Global.gridSize)*Global.gridSize,
										floor(get_global_mouse_position().y/Global.gridSize)*Global.gridSize)
