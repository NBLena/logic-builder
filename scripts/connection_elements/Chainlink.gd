extends Node2D

var id = 0
var SumObject:Label = null
var editMode := false
var line:Line2D = null
export var disabled := false
var sumObjectRes = load("res://scenes/elements/Sum.tscn")

func _ready()->void:
	add_to_group("connection_nodes")
	id = Global.getNextId()

func _input(event):
	if editMode:
		if Global.currentlyDraggedObject != null:
			exitEditMode()
			remove_child(line)
		if event is InputEventMouseMotion:
			# Set line to mouse position
			line.set_point_position(1, (get_global_mouse_position() - global_position)*2)
		elif event.is_action_pressed("ui_delete"):
			# Unselect this Output
			exitEditMode()
			remove_line()

func _on_Area2D_input_event(_viewport, event, _shape_idx)->void:
	if disabled:
		return

	if event.is_action_pressed("ui_accept") and not editMode:
		if Global.selectedOutput != null and Global.selectedOutput.name == "Chainlink":
			if SumObject != null:
				return
			connect_chainlink()
			Global.selectedOutput.remove_line()
			Global.selectedOutput.exitEditMode()
			return
		enterEditMode()
		
	if event.is_action_pressed("ui_delete"):
			remove_line()
			unregister()

func enterEditMode()->void:
	if (Global.selectedOutput == null):
		line = Line2D.new()
		line.add_point(Vector2(0,0))
		line.add_point(Vector2(0,0))
		line.default_color = Color(0.780392, 0, 0.223529)
		add_child(line)
		Global.selectedOutput = self
		editMode = true
		#disable hiding of this chainlink
		get_parent().get_node("HoverArea").disabled = true

func exitEditMode()->void:
	editMode = false
	Global.selectedOutput = null
	#re-enable hiding of this chainlink
	get_parent().get_node("HoverArea").disabled = false
	# TODO: check if mouse is within Hoverarea, and if yes, do nothing
	self.visible = false

func remove_line()->void:
	if line != null:
		remove_child(line)
		line = null

func connect_chainlink()->void:
	# If Sum object exists... register yourself there
	if Global.selectedOutput.SumObject != null:
		SumObject = Global.selectedOutput.SumObject
		SumObject.register(get_parent())
		#get_parent().passToChainlink(true)
		return
	# If not connected to a Sum object... create it
	SumObject = sumObjectRes.instance()
	SumObject.set_global_position(get_global_position() + Vector2(40,0))
	get_tree().root.get_node("Global").add_child(SumObject)
	# Share instance reference to first selected lamp too
	Global.selectedOutput.SumObject = SumObject
	# ... register the lamp that was first selected
	SumObject.register(Global.selectedOutput.get_parent())
	#Global.selectedOutput.get_parent().passToChainlink(true)
	# ... register this lamp
	SumObject.register(get_parent())
	#get_parent().passToChainlink(true)

func unregister()->void:
	if SumObject != null:
		SumObject.unregister(get_parent())
		get_parent().get_node("SumValueLabel").visible = false

func updateLine()->void:
	if SumObject == null:
		return
	SumObject.updateLines(get_parent())

func getSumObjectId()->int:
	if SumObject == null:
		return 0
	return SumObject.id

# Prepare for deletion
func selfDestruct()->void:
	self.id = null
