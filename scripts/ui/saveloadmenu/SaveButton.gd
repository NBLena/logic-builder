extends Button

onready var saveNameField = $"..".get_node("SaveNameField")

func _on_SaveButton_pressed():
	var newSaveName = "user://" + saveNameField.text
	SaveManager.save_game(newSaveName)
	get_parent().queue_free()
