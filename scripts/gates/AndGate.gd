extends GateBase

func _ready()->void:
	if not disabled:
		inputs.resize(2)
		inputs[0] = false
		inputs[1] = false
		updateOutput()

func _on_Input_A_recieved(input_on:bool)->void:
	inputs[0] = input_on
	updateOutput()

func _on_Input_B_recieved(input_on:bool)->void:
	inputs[1] = input_on
	updateOutput()

func transform()->bool:
	return inputs[0] and inputs[1]
