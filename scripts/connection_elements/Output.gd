extends Node2D

signal transmit(on)
var editMode := false
var line:Line2D = null
var lines := []
var connectedObjects := []
var time := 2.0
export var lastSignal := false
export var disabled := false

func _ready()->void:
	add_to_group("connection_nodes")

func _process(delta):
	if(time/2 < 1):
		time += delta
		self.modulate = lerp(Color(1, 0.003922, 0.003922), Color(1,1,1), time / 2)

func submit_change(input_data)->void:
	if input_data is bool:
		emit_signal("transmit", input_data)
		lastSignal = input_data
	elif input_data is Array:
		for object in connectedObjects:
			object.call("_on_recieve", input_data.duplicate(true)) # Propagate to each connected Input one by one, instead of shotgun approach
	else:
		print("ERROR: Type of data passed on via signal is not legal!")

func repeat_signal()->void:
	emit_signal("transmit", lastSignal)

func _on_Area2D_input_event(_viewport, event, _shape_idx)->void:
	if disabled:
		return
	if event.is_action_pressed("ui_accept") and not editMode:
		enterEditMode()
	if event.is_action_pressed("ui_delete"):
			if line != null:
				disconnectAll()

func _input(event)->void:
	if editMode:
		if Global.currentlyDraggedObject != null:
			exitEditMode()
			remove_child(line)
		if event is InputEventMouseMotion:
			# Set line to mouse position
			line.set_point_position(1, (get_global_mouse_position() - global_position)*2)
		elif event.is_action_pressed("ui_delete"):
			# Unselect this Output
			exitEditMode()
			remove_child(line)

func finishConnection(target)->void:
	connectedObjects.append(target)
	if connect("transmit", target, "_on_recieve") != OK:
		print("ERROR: Output could not connect transmit Signal to Input: ", self, " to ", target)
		return
	exitEditMode()
	line.set_point_position(1, (target.global_position - global_position)*2)
	lines.append(line)
	target.call("_on_recieve", []) # check for cycles, avoid signal to only test the connected input
	repeat_signal()

func disconnectAll()->void:
	emit_signal("transmit", "disconnect")
	for i in range(0, connectedObjects.size()):
		disconnect("transmit", connectedObjects[i], "_on_recieve")
	connectedObjects.clear()
	for i in range(0, lines.size()):
		remove_child(lines[i])
	lines.clear()

func disconnectOne(targetInput)->void:
	disconnect("transmit", targetInput, "_on_recieve")
	var toDelete:int = connectedObjects.find(targetInput)
	connectedObjects.remove(toDelete)
	remove_child(lines[toDelete])
	lines.remove(toDelete)
	
func enterEditMode()->void:
	if (Global.selectedOutput == null):
		line = Line2D.new()
		line.add_point(Vector2(0,0))
		line.add_point(Vector2(0,0))
		line.default_color = Color(0.972549, 0.698039, 0.309804)
		add_child(line)
		Global.selectedOutput = self
		editMode = true

func exitEditMode()->void:
	editMode = false
	Global.selectedOutput = null
	
func gatherIds()->Array:
	var ids := []
	for object in connectedObjects:
		if (is_instance_valid(object) and not object.disabled): # Should be false if object has been freed
			ids.append(object.id)
	return ids

func reconnectAll(inputIds: Array)->void:
	#Find the nodes we want to connect to
	var inputs := []
	for input in get_tree().get_nodes_in_group("inputs"):
		if is_instance_valid(input) and inputIds.has(input.id):
			inputs.append(input)
	
	for input in inputs:
		Global.linkConnectionElements(self, input)

func _on_response(sender, data)->void:
	var senderIndex:int = connectedObjects.find(sender)
	if data is Vector2: #Probably the position of the Input node then
		lines[senderIndex].set_point_position(1, (data - global_position)*2)
		
func alertFlash()->void:
	self.modulate = Color(1, 0.003922, 0.003922)
	time = 0
