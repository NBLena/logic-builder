extends Polygon2D

export var active = false

func _on_Area2D_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("ui_accept"):
		if active:
			active = false
			self.color = Color(0.05098, 0.035294, 0.14902)
		else:
			active = true
			self.color = Color(0.152941, 0.509804, 0.615686)
		
		for node in get_tree().get_nodes_in_group("connection_nodes"):
			if active:
				node.get_child(0).color.a = 0
			else:
				node.get_child(0).color.a = 1
		for node in get_tree().get_nodes_in_group("hideable"):
			if active:
				node.color.a = 0
			else:
				node.color.a = 1
