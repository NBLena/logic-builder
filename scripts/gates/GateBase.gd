extends Polygon2D

class_name GateBase

var inputs := []
var output := false
export var disabled := false
signal process(output)

func _ready()->void:
	if disabled:
		for child in get_children():
			if child.get("disabled") != null:
				child.disabled = true

func updateOutput()->void:
	output = transform()
	emit_signal("process", output)

func transform()->bool:
	print("ERROR: transform() is not overriden in ", self.name, "!")
	return false

func save()->Dictionary:
	return {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"connectedObjects": $Output.gatherIds(),
		"inputObjectIds": gatherInputIds()
	}

func gatherInputIds()->Array:
	var ids = []
	ids.append($Input.id)
	for i in range(1,inputs.size()):
		ids.append(get_node(str("Input",i+1)).id)
	return ids
