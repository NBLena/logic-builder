extends Button

onready var fileListBox = $"..".get_node("FileListBox")

func _on_DeleteButton_pressed():
	var selectedLine = fileListBox.cursor_get_line()
	var selectedFileName = fileListBox.get_line(selectedLine)
	var dir = Directory.new()
	if dir.remove("user://" + selectedFileName) != OK:
		print("ERROR: Could not remove ", selectedFileName)
	$"..".readFileList()
	fileListBox.cursor_set_line(selectedLine)


func _on_FileListBox_gui_input(event):
	if event.is_action_pressed("ui_delete"):
		_on_DeleteButton_pressed()
