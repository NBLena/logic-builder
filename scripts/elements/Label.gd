extends Control

export var disabled := false
var text:String

func save()->Dictionary:
	return {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : rect_position.x, # Vector2 is not supported by JSON
		"pos_y" : rect_position.y,
		"text": text
	}

func _on_LineEdit_text_changed(new_text):
	self.text = new_text

# This gets called when loading a circuit
func updateOutput()->void:
	$LineEdit.text = text
