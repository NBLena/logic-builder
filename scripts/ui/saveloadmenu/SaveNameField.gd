extends LineEdit

onready var saveButton = $"..".get_node("SaveButton")
onready var fileListBox = $"..".get_node("FileListBox")

func _ready():
	if Global.savefileName != null:
		text = Global.savefileName.trim_prefix("user://")
		saveButton.disabled = false

func _on_SaveNameField_text_changed(new_text):
	if new_text == "":
		saveButton.disabled = true
		return
	saveButton.disabled = false


func _on_FileListBox_cursor_changed():
	if $initTimer.time_left > 0:
		return
	text = fileListBox.get_line(fileListBox.cursor_get_line())
	saveButton.disabled = false
